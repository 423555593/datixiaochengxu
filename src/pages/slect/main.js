/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-04 16:18:08
 * @LastEditTime: 2019-09-04 16:19:22
 * @LastEditors: Please set LastEditors
 */
import App from './index'
import Vue from 'vue'

const app = new Vue(App)
app.$mount()